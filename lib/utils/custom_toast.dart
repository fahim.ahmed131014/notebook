import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';

class CustomToast{
    static void  toastShower(String msg, Color color){
      Fluttertoast.showToast(
        msg: msg,
        fontSize: 20,
        timeInSecForIosWeb: 1,
        textColor: Colors.black,
        backgroundColor: color,
        gravity: ToastGravity.BOTTOM,
        toastLength: Toast.LENGTH_SHORT,
      );
    }
}