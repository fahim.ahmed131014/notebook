import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:notebook/models/date_formatter.dart';
import 'package:notebook/models/notebook_info.dart';
import 'package:notebook/repository/notebook_repository.dart';
import 'package:provider/provider.dart';

class NoteBookProvider extends ChangeNotifier{
  String? _date ;

  List<NoteBookInfo> _noteBookInfoList = [];
  List<NoteBookInfo> _noteBookInfoFilterList =[];



  Future<String> insertData( NoteBookInfo notebookInfo)async{
    String status = "loading";

    notifyListeners();
    int value = await NoteBookRepository().insertData(notebookInfo);
    if (value == 1){
      status = "success";
    }
    else{
      status = "error";
    }

    notifyListeners();
    return status;
  }

  Future<String> updateData( NoteBookInfo notebookInfo)async{
    String status = "loading";

    notifyListeners();
    int value = await NoteBookRepository().updateData(notebookInfo);
    if (value == 1){
      status = "success";
    }
    else{
      status = "error";
    }

    notifyListeners();
    return status;
  }


  Future<void> fetchData( )async{

    notifyListeners();
    _noteBookInfoList = await NoteBookRepository().fetchData();
    _noteBookInfoFilterList = await NoteBookRepository().fetchData();
    notifyListeners();

  }


  Future<String> deleteData(int id)async{
    String status = "loading";

    notifyListeners();
    int value = await NoteBookRepository().deleteData(id);
    if (value == 1){
      status = "success";
    }
    else{
      status = "error";
    }

    notifyListeners();
    return status;
  }

  Future<String> deleteTable(String tableName)async{
    String status = "loading";

    notifyListeners();
    int value = await NoteBookRepository().deleteTable(tableName);
    if (value == 1){
      status = "success";
    }
    else{
      status = "error";
    }

    notifyListeners();
    return status;
  }




  List<NoteBookInfo> get noteBookInfoList => _noteBookInfoList;

  set noteBookInfoList(List<NoteBookInfo> value) {
    _noteBookInfoList = value;
    notifyListeners();
  }



  String get date => _date!;

  set date(String value) {
    _date = value;
    notifyListeners();
  }



  List<NoteBookInfo> get noteBookInfoFilterList => _noteBookInfoFilterList;

  set noteBookInfoFilterList(List<NoteBookInfo> value) {
    _noteBookInfoFilterList = value;
    notifyListeners();
  }
}