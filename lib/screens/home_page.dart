import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:notebook/common_widget/appname_widget.dart';
import 'package:notebook/models/notebook_info.dart';
import 'package:notebook/provider/notebook_provider.dart';
import 'package:notebook/screens/add_note.dart';
import 'package:notebook/screens/drawer_page.dart';
import 'package:notebook/screens/update_note.dart';
import 'package:notebook/utils/custom_toast.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}




class _HomePageState extends State<HomePage> {
  String? greetings;


  @override
  void initState() {
    super.initState();
    timeNow();
  }



  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    NoteBookProvider noteBookProvider = await Provider.of<NoteBookProvider>(context, listen: false);
    noteBookProvider.fetchData();
  }






  void timeNow(){
    dynamic timeOfNow = DateTime.now().hour;
    if(timeOfNow >=0 && timeOfNow<=6){
      greetings = "Good Night";
    }
    else if(timeOfNow>6 &&timeOfNow<12)
    {
      greetings = "Good Morning";
    }
    else if(timeOfNow>=12 &&timeOfNow<=15)
    {
      greetings = "Good Noon";
    }
    else if(timeOfNow>15 &&timeOfNow<18)
    {
      greetings = "Good Afternoon";
    }
    else {
      greetings = "Good Evening";
    }
  }




  void showSelectedPopUpMenuBar (String value, NoteBookProvider noteBookProvider, int id, NoteBookInfo noteBookInfo) async{
    switch(value){
      case "delete":
        alertBox(id, noteBookProvider);
        break;

      case "update":
        bool isUpdated = await Navigator.push(context, MaterialPageRoute(builder: (context){
          return UpdateNote(noteBookInfo);
        }),
        );
        if(isUpdated == true){
          noteBookProvider.fetchData();
        }
        break;
    }
  }





  void alertBox(int id, NoteBookProvider noteBookProvider){
    Alert(
      context: context,
      title: "Delete Note",
      desc: "Do you want to delete this note?",
      buttons: [
        DialogButton(
            color: Colors.green,
            child: const Text("Yes",
            style: TextStyle(
              fontSize:20,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
            ),
            onPressed: ()async{
              String value = await noteBookProvider.deleteData(id);
              CustomToast.toastShower("Note has deleted successfully",Colors.green);
              noteBookProvider.fetchData();
              Navigator.pop(context);
            }),
        DialogButton(
            color: Colors.red,
            child: const Text("No",
              style: TextStyle(
                fontSize:20,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            onPressed: (){
              CustomToast.toastShower("Note has not deleted", Colors.red);
              noteBookProvider.fetchData();
              Navigator.pop(context);
            }),
      ]
    ).show();
  }



  void searchNote(String value, NoteBookProvider noteBookProvider){
    noteBookProvider.noteBookInfoList.clear();
    if(value.isNotEmpty){
      List<NoteBookInfo> searchList = [];
      for(NoteBookInfo noteBookInfo in noteBookProvider.noteBookInfoFilterList){
        if(noteBookInfo.title.toLowerCase().contains(value.toLowerCase()) ||
            noteBookInfo.content.toLowerCase().contains(value.toLowerCase()) ||
        noteBookInfo.date.toLowerCase().contains(value.toLowerCase())
          ){
          searchList.add(noteBookInfo);
        }
        noteBookProvider.noteBookInfoList = searchList;
      }
    }else{
      noteBookProvider.noteBookInfoList = noteBookProvider.noteBookInfoFilterList;
    }
  }



  @override
  Widget build(BuildContext context) {
    return Consumer<NoteBookProvider>(
      builder: (_,provider,___) {
        return Scaffold(
          floatingActionButton: Container(
            margin: const EdgeInsets.only(right:20,bottom: 20),
            child:FloatingActionButton(
              backgroundColor: Colors.blue,
              onPressed: () async {
              bool isAdded = await Navigator.push(context,
                    MaterialPageRoute(builder: (context) {
                         return AddNote();
                  }
                )
              );
              if (isAdded == true) {
                provider.fetchData();
              }
            },
            child: Icon(FontAwesomeIcons.plus),
            ),
          ),
          drawer: DrawerPage(),
          appBar: AppBar(
            toolbarHeight: 60,
            title: const AppNameWidget(),
            centerTitle: true,
          ),

          body: SingleChildScrollView(
            child: SafeArea(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 50,
                    ),
                    const Icon(
                      FontAwesomeIcons.book,
                      size:30 ,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    const Text.rich(
                      TextSpan(
                          children: [
                            TextSpan(
                              text: "Hi, ",
                              style: TextStyle(
                                fontSize:20,
                              ),
                            ),
                            TextSpan(
                              text: "Fahim Ahmed ",
                              style: TextStyle(
                                fontSize:20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ]
                      ),
                    ),
                    const SizedBox(
                      height:8,
                    ),
                    greetings != null ?
                    Text("$greetings!!",
                      style: TextStyle(
                        fontSize: 15,

                      ),
                    ):const Text(""),
                    SizedBox(
                      height:10,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextField(
                        onChanged: (value) {
                           searchNote(value, provider);
                        },
                        decoration: const InputDecoration(
                          labelText: 'search by title...',
                          prefixIcon: Icon(Icons.search),
                          fillColor: Colors.white30,
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blueAccent),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(20),
                            ),
                            borderSide: BorderSide(
                              color: Colors.blue,
                            ),
                          ),

                          contentPadding: EdgeInsets.only(
                              bottom: 10.0, left: 10.0, right: 10.0),
                        ),


                      ),
                    ),

                    provider.noteBookInfoList.isEmpty ? const Center(
                      child: CircularProgressIndicator(
                        valueColor:AlwaysStoppedAnimation<Color>(Colors.blue),
                      ),
                    ): ListView.separated(
                        itemCount: provider.noteBookInfoList.length,
                        separatorBuilder: (context,index)=>
                           const SizedBox(
                              height:5,
                            ),
                        shrinkWrap: true,
                        physics:  NeverScrollableScrollPhysics(),
                        itemBuilder: (context,index){
                          return Container(
                            margin: const EdgeInsets.only(
                                left: 20.0, right: 20),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.blue,
                              ),
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: <Widget>[
                                      Text(
                                        provider.noteBookInfoList[index].title,
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        provider.noteBookInfoList[index].content,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        provider.noteBookInfoList[index].date,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontSize: 15,
                                        ),
                                      ),
                                    ],
                                  ),
                                  PopupMenuButton<String>(
                                    icon: Icon(Icons.more_vert),
                                    onSelected: (value) {
                                      showSelectedPopUpMenuBar(value, provider, provider.noteBookInfoList[index].id, provider.noteBookInfoList[index]);
                                    },
                                    itemBuilder: (BuildContext context) =>
                                    [
                                      const PopupMenuItem(
                                          value: 'update',
                                          child: ListTile(
                                              leading: Icon(Icons.edit),
                                              title: Text('Update')
                                          )
                                      ),
                                      const PopupMenuItem(
                                          value: 'delete',
                                          child: ListTile(
                                              leading: Icon(Icons.delete),
                                              title: Text('Delete')
                                          )
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          );
                        }
                    ),

                  ],
                ),
              ),
            ),
          ),


        );
      }
    );

  }
}


