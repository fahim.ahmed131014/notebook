import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:notebook/common_widget/appname_widget.dart';
import 'package:notebook/database/database_helper.dart';
import 'package:notebook/models/date_formatter.dart';
import 'package:notebook/models/notebook_info.dart';
import 'package:notebook/provider/notebook_provider.dart';
import 'package:notebook/utils/custom_toast.dart';
import 'package:provider/provider.dart';

class AddNote extends StatefulWidget {

  @override
  _AddNoteState createState() => _AddNoteState();
}

class _AddNoteState extends State<AddNote> {
  String? date;

  TextEditingController _titleController = TextEditingController();
  TextEditingController  _contentController = TextEditingController();



  @override
  void initState() {
    super.initState();
    date =  DateFormatter.getDateInFormat(DateTime.now().toString().substring(0,10));
  }




   addNote(NoteBookProvider noteBookProvider)async{
    if(_titleController.text == ""){
      CustomToast.toastShower("Add Title", Colors.red);
    }
    else if(_contentController.text == "")
      {
        CustomToast.toastShower("Add Content",Colors.red);
      }
    else if(date == null){
      CustomToast.toastShower("Add Date",Colors.red);
    }
    else{
      NoteBookInfo noteBookInfo = NoteBookInfo(
        title: _titleController.text,
        content: _contentController.text,
        date: date,
      );
       var isAdded = await noteBookProvider.insertData(noteBookInfo);
       if(isAdded != null) {
         CustomToast.toastShower("Successfully Added", Colors.green);
         Navigator.pop(context, true);
       }
       else{
         CustomToast.toastShower("Not Added", Colors.red);
         Navigator.pop(context, false);
       }
    }
  }


  @override
  Widget build(BuildContext context) {
    return Consumer<NoteBookProvider>(
        builder: (_, provider, ___) {
          return Scaffold(
            appBar: AppBar(
              title: Text("Add Note"),
              centerTitle: true,
              leading: InkWell(
                onTap: () {
                   Navigator.pop(context);
                },
                child: Icon(FontAwesomeIcons.backward),
              ),
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 25.0, top: 30),
                      child: Text("Title*",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20, left: 25, right: 25),
                      child: TextFormField(
                        controller:_titleController,
                        decoration: InputDecoration(
                          hintText: "write note title",
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 25.0, top: 30),
                      child: Text("Content*",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20, left: 25, right: 25),
                      child: TextFormField(
                        controller: _contentController,
                        decoration: InputDecoration(
                          hintText: "write note content",
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 25.0, top: 30),
                      child: Text("Date*",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20, left: 25, right: 25),
                      child: Container(
                        decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),
                         color: Colors.white54,
                        ),
                        child: ListTile(
                          title: Text(date!),
                          trailing: Icon(FontAwesomeIcons.calendar),
                           onTap: () {
                            showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(1970),
                              lastDate: DateTime(2070),
                            ).then((DateTime? value) =>{
                              date = DateFormatter.getDateInFormat(
                                     value.toString().substring(0, 10)),
                            } );
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 80,
                    ),
                    Center(
                      child: Container(
                        width: 200,
                        //margin: EdgeInsets.only(left:50,right:50),
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: TextButton(
                            onPressed: (){
                                addNote(provider);
                            },
                            child: Text("Add Note",
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              ),
                            ),
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ),
          );
        }
    );
  }
}
