import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:notebook/common_widget/appname_widget.dart';
import 'package:notebook/common_widget/drawer_item.dart';

class DrawerPage extends StatefulWidget {
  const DrawerPage({Key? key}) : super(key: key);

  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height:double.infinity,
      width: 300,
      child: Scaffold(
        backgroundColor: Colors.blue,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.only(top:80.0,left:28),
                child: AppNameWidget(),
              ),
              SizedBox(
                height:100,
              ),
              Drawer_Item(
                drawerText: "About Us".toUpperCase(),
                drawerIcon: FontAwesomeIcons.info,
                onPressed: (){

                },
              ),
              SizedBox(
                height:30,
              ),
              Drawer_Item(
                drawerText: "Delete Account".toUpperCase(),
                drawerIcon: FontAwesomeIcons.cross,
                onPressed: (){

                },
              ),
              SizedBox(
                height:30,
              ),
              Drawer_Item(
                drawerText: "Log Out".toUpperCase(),
                drawerIcon: FontAwesomeIcons.backward,
                onPressed: (){

                },
              ),

            ],
          ),
        ),
      ),
    );
  }
}


