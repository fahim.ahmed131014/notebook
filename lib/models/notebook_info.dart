class NoteBookInfo{
  dynamic id;
  dynamic title;
  dynamic content;
  dynamic date;

  NoteBookInfo({this.id, this.title, this.content, this.date});

  Map<String, dynamic> toMap() {
    return <String, dynamic> {
      "id" : id,
      "title" : title,
      "content" : content,
      "date" : date
    };
  }
}