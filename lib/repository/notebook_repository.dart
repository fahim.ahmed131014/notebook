import 'package:notebook/database/database_helper.dart';
import 'package:notebook/models/notebook_info.dart';

class NoteBookRepository{


  //insertData
  Future<int> insertData( NoteBookInfo notebookInfo)async{
    return  DatabaseHelper().insertData(notebookInfo);
  }

  //fetchData
  Future<List<NoteBookInfo>> fetchData()async{
    return  DatabaseHelper().fetchData();
  }

  Future<int> updateData(NoteBookInfo noteBookInfo) async{
    return  DatabaseHelper().updateData(noteBookInfo);
  }

  //deleteData
  Future<int> deleteData(int id)async{
    return  DatabaseHelper().deleteData(id);
  }

  //deleteTable
  Future<int> deleteTable(String tableName)async{
    return  DatabaseHelper().deleteTable(tableName);
  }



}