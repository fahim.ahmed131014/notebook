import 'dart:io';
import 'package:notebook/utils/constants.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:notebook/models/notebook_info.dart';

class DatabaseHelper{

  Future<Database> initDatabase() async{

    Directory  directory = await getApplicationDocumentsDirectory();
    final path = join(directory.path,'notebook.db');
    return await openDatabase(
        path,
        version: 1,
        onCreate: (Database db, int version) async{
        await db.execute(
          "CREATE TABLE " + Constants.TABLE_NAME + "(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT,content TEXT, date TEXT)"
          "");
      }
    );

  }

  Future<int> insertData( NoteBookInfo notebookInfo)async{
    final db = await initDatabase();
    return db.insert(Constants.TABLE_NAME,
      notebookInfo.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore
    );
  }

  Future<int> updateData( NoteBookInfo notebookInfo)async{
    final db = await initDatabase();
    return db.update(Constants.TABLE_NAME,
        notebookInfo.toMap(),
        where: "id = ?",whereArgs: [notebookInfo.id]
    );
  }



  Future<List<NoteBookInfo>> fetchData( )async{
    final db = await initDatabase();
    final maps = await db.query(Constants.TABLE_NAME);
    return List.generate(maps.length, (index) {
      return NoteBookInfo(
          id : maps[index]['id'],
          title: maps[index]['title'],
          content: maps[index]['content'],
          date: maps[index]['date'],
      );
    });
  }


  Future<int> deleteData (int id)async{
    final db = await initDatabase();
    return db.delete(Constants.TABLE_NAME ,
    where: "id = ?",whereArgs: [id]);
  }
  Future<int> deleteTable (String tableName)async{
    final db = await initDatabase();
    return db.delete(tableName);
  }



}